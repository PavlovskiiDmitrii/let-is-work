import {axios} from "../../core/";

export default {
    getUserToken: (userPassAndEmailObj) => {
        return axios.post("authorization/signin/", userPassAndEmailObj)
    },
    userSignUp: (userPassAndEmailObj) => axios.post("authorization/signup/", userPassAndEmailObj)
};
