import { axios } from "../../core/";

const headers = {
    'Authorization': 'Token 4d563da9b9a5ca68c94524937a35d2cadca50445'
}

export default {
    getProject: (userToken) => axios.get("project/", {
        headers: headers
    })
};
