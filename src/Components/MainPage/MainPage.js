import React from 'react';
import Header from '../Common/Header/Header';
import MainContent from '../MainContent/MainContent';
import SideMenu from '../SideMenu/SideMenu';

import './MainPage.scss'

const MainPage = (props) => {

    return ( 
        <div className="mainpage">
            <Header/>
            <div className="mainpage__content">
                <SideMenu/>
                <MainContent/>
            </div>
        </div>
     );
}
 
export default MainPage;
