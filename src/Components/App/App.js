import React from 'react';
import PopUp from '../PopUp/PopUp';
import WrapPage from '../WrapPage/WrapPage.js';

import './App.scss'

const App = () => {

    return ( 
        <div className="app">
            <WrapPage/>
            <PopUp/>
        </div>
     );
}

export default App;
