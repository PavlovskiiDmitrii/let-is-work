import React, { useEffect } from 'react';
import LandPage from '../LandPage/LandPage.js';
import MainPage from '../MainPage/MainPage.js';
import Auth from '../Hocs/Auth/Auth';

import './WrapPage.scss'

const WrapPage = (props) => {

    const {auth, createUser, getUserToken} = props;

    return ( 
        <>
            {
                auth
                ?
                <MainPage/>
                :
                <LandPage createUser={createUser} getUserToken={getUserToken}/>
            }
        </>
     );
}
 
export default Auth(WrapPage);
