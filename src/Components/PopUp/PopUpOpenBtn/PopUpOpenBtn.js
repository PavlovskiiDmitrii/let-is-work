import React from 'react';
import PopUpBtnHOC from '../../Hocs/PopUpBtn/PopUpBtn';

const PopUpOpenBtn = (props) => {

    const {text, Component} = props;

    return ( 
        <button onClick={() => { props.togglePopUp(Component) }}>
            {text}
        </button>
     );
}
 
export default PopUpBtnHOC(PopUpOpenBtn);
