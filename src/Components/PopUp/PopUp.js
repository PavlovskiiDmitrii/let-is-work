import React, {useEffect, useState} from 'react';

import './PopUp.scss'

const PopUp = (props) => {

    const [Component, setComponent] = useState('');
    const [open, setOpen] = useState(false);

    const openPopUp = () => {
        e.preventDefault();
        console.log(123)

        window.ee.emit('News.add');
    }

    useEffect(() => {
        window.ee.addListener('News.add', function(item) {
            setComponent(item)
            setOpen(true)
          });
    }, [])
   


    return ( 
        <div className={`popup__wrap ${open && 'popup__wrap_open'}`}>
            <div className="popup" >
                {Component}
                <button onClick={() => {
                    setOpen(false)
                }}>
                    close
                </button>
            </div>
        </div>
     );
}
 
export default PopUp;
