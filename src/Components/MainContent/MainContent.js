import React from 'react';
import TestBtn from '../TestBtn/TestBtn';

import './MainContent.scss'

const MainContent = (props) => {

    return ( 
        <div className="mainContent__wrap">
            <div className="mainContent">
                <TestBtn/>
            </div>
        </div>
     );
}
 
export default MainContent;
