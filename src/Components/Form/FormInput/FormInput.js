import React, {useState} from 'react';
import Input from '../../Hocs/Input/Input';

import inputValid from '../../../Utils/Validations';

import './FormInput.scss'

const FormInput = (props) => {

    const {text, type, valueType, onChangeInput} = props;

    const [ valid, setValid ] = useState(true);

    return (
        <div className={`formInput ${!valid && 'formInput_error'}`}>
            <label>
                {text}:
                <input type={type} name="name" onChange={ (e) => { 
                        onChangeInput(() => { 
                            setValid(inputValid[valueType](e.target.value))
                        }) 
                    } }/>
            </label>  
        </div>
     );
}

export default Input(FormInput);
