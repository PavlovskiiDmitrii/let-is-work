import React from 'react';
import Form from '../../Hocs/Form/Form';

import './FormTemplate.scss'

const FormTemplate = (props) => {
    const {authFun, onSubmit} = props;

    const funcOnAuthorCreateUser = (e, authFun, onSubmitFunc) => {
        let email = e.target.querySelectorAll('input[type=text]')[0].value;
        let pass = e.target.querySelectorAll('input[type=password]')[0].value;

        onSubmitFunc(() => {authFun({ "password" : pass, "email": email})});
    }

    const onSubmitLocal = (e) => {
        e.preventDefault();
        funcOnAuthorCreateUser(e, authFun, onSubmit);
    }

    return (
        <form onSubmit={(e) => {onSubmitLocal(e)}}>
            {props.children}
        </form>
     );
}

export default Form(FormTemplate);
