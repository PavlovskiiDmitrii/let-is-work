import React from 'react';
import { debounce } from 'lodash'

const Input = (InnerComponent) => {

    const HOC = (props) => {
        
        const onChangeInput = debounce((fun) => {
            fun();
            console.log('onChangeInput');
        }, 500)

        return (
            <InnerComponent
                {...props}
                onChangeInput={onChangeInput}
            />
        );
    }

    return HOC;
}

export default Input;