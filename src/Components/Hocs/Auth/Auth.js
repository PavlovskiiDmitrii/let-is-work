import React, { Component } from 'react';
import {connect} from 'react-redux';

import { userActions } from '../../../Redux/actions';


const Auth = (InnerComponent) => {

    class HOC extends Component {
        constructor(props) {
            super(props);
            this.state = {
                auth: this.props.user.auth
            };

            this.createUser = this.createUser.bind(this);
            this.getUserToken = this.getUserToken.bind(this);
        }

        createUser(userPassAndEmailObj) {
            this.props.userCreateAndAuthorization(userPassAndEmailObj);
        }

        getUserToken(userPassAndEmailObj) {
            this.props.getUserToken(userPassAndEmailObj);
        }

        componentDidUpdate(prevProps, prevState) {
            if (prevProps.user.auth !== this.props.user.auth) {
                this.setState({
                    auth: this.props.user.auth
                })
            }
        }
        
        render() {
            return (
                <InnerComponent
                    {...this.props}
                    createUser={this.createUser}
                    getUserToken={this.getUserToken}
                    auth={this.state.auth}
                />
            );
        }
    }

    return connect(({
        user
    }) => ({
        user: user
    }), {
        ...userActions
    })(HOC);
}

 
export default Auth;

