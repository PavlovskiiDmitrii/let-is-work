import React, { Component } from 'react';
import {connect} from 'react-redux';

import { userActions } from '../../../Redux/actions';


const UnAuthBtn = (InnerComponent) => {

    class HOC extends Component {
        constructor(props) {
            super(props);
            this.state = {
                auth: '111'
            };

            this.unAuthUser = this.unAuthUser.bind(this);
        }

        unAuthUser () {
            this.props.unSetUser();
            localStorage.removeItem('user')
        }

        render() {
            return (
                <InnerComponent
                    {...this.props}
                    unAuthUser={this.unAuthUser}
                />
            );
        }
    }

   return connect(({
      user
   }) => ({
         user: user
   }), {
         ...userActions
   })(HOC);
}

 
export default UnAuthBtn;


