import React, { Component } from 'react';

const PopUpBtn = (InnerComponent) => {

    class HOC extends Component {
        constructor(props) {
            super(props);
            this.togglePopUp = this.togglePopUp.bind(this);
        }

        togglePopUp(item) {
            window.ee.emit('News.add', item);
        }
        
        render() {
            return (
                <InnerComponent
                    {...this.props}
                    togglePopUp={this.togglePopUp}
                />
            );
        }
    }

    return HOC;
}

export default PopUpBtn;

