import React from 'react';
import { debounce } from 'lodash'

const Form = (InnerComponent) => {

    const HOC = (props) => {
        
        const onSubmit = (fun) => {
            fun();
            console.log('onSubmit');
        }

        return (
            <InnerComponent
                {...props}
                onSubmit={onSubmit}
            />
        );
    }

    return HOC;
}

export default Form;