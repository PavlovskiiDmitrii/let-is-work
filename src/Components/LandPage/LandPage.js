import React from 'react';
import FormInput from '../Form/FormInput/FormInput';
import FormTemplate from '../Form/FormTemplate/FormTemplate';
import PopUpBtn from '../PopUp/PopUpOpenBtn/PopUpOpenBtn';

import './LandPage.scss'

const LandPage = props => {

    const {createUser, getUserToken} = props;
   
    return ( 
        <div className="landpage">
            <PopUpBtn text={'Создать аккаунт'} Component={
                <FormTemplate authFun={ createUser }>
                    <FormInput text={"email"} type={'text'} valueType={'email'}/>
                    <FormInput text={"Пароль"} type={'password'} valueType={'password'}/>
                    <button type="submit">Создать пользователя</button>
                </FormTemplate>
            }/>
            <PopUpBtn text={'Авторизоваться2'} Component={
                 <FormTemplate authFun={ getUserToken }>
                    <FormInput text={"email"} type={'text'} valueType={'email'} />
                    <FormInput text={"Пароль"} type={'password'} valueType={'password'}/>
                    <button type="submit">Авторизоваться</button>
                </FormTemplate>
            }/>
        </div>
     );
}
 
export default LandPage;