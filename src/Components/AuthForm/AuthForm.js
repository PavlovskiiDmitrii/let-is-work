import React from 'react';
import UnAuth from '../Hocs/UnAuthBtn/UnAuthBtn';

import './AuthForm.scss'

const TestUnAuthBtn = UnAuth((props) => {
    return ( 
        <button onClick={() => {props.unAuthUser()}}>
            Анлокинг
        </button>
     );
})

const AuthForm = (props) => {
    return ( 
        <div className="authForm">
            <TestUnAuthBtn/>
        </div>
     );
}
 
export default AuthForm;