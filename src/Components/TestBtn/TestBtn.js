import React from 'react';
import PopUpBtn from '../Hocs/PopUpBtn/PopUpBtn';
import AuthForm from '../AuthForm/AuthForm';

const TestBtn = (props) => {
    return ( 
        <button onClick={() => {props.togglePopUp(<AuthForm/>)}}>
            Авторизация
        </button>
     );
}
 
export default PopUpBtn(TestBtn);
