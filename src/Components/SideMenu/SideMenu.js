import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';

import { projectActions } from '../../Redux/actions';

import './SideMenu.scss'

const SideMenu = (props) => {
    const { user, project, fetchProject } = props;

    const [auth, setAuth] = useState(user.auth);

    useEffect(() => {
        if (auth) {
            fetchProject(user.token)
        }
    }, [])

    return (
        <div className={`sideMenu`}>
            Projects <br/><br/>
            <div>
                {
                    project &&
                    Object.values(project).map((item) => (
                        <div key={item.id}>{item.name}</div>
                    ))
                }
            </div>
        </div>
    );
}

export default connect(({
    user,
    project
}) => ({
    user: user,
    project: project
}), {
    ...projectActions
})(SideMenu);