import { user } from '../../Utils/api';

const Actions = {
    setUser: (items) => ({
        type: 'USER:SET_ME',
        payload: items
    }),
    unSetUser: () => ({
        type: 'USER:UNSET',
        payload: {}
    }),
    userCreateAndAuthorization : (userPassAndEmailObj) => dispatch => {
        user.userSignUp(userPassAndEmailObj).then(() => {
            user.getUserToken(userPassAndEmailObj).then(({data}) => {
                dispatch(Actions.setUser(data));
                localStorage.setItem("user", JSON.stringify({...data, auth: true }));
            })
        })
    },
    getUserToken : (userPassAndEmailObj) => dispatch => {
        user.getUserToken(userPassAndEmailObj).then(({data}) => {
            dispatch(Actions.setUser(data));
            localStorage.setItem("user", JSON.stringify({...data, auth: true }));
        });
    }
}

export default Actions;
