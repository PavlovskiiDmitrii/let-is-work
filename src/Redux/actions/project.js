import { user, project } from '../../Utils/api';

const Actions = {
    setProject: (items) => ({
        type: 'PROJECT:SET',
        payload: items
    }),
    fetchProject : (userToken) => dispatch => {
        project.getProject(userToken).then(({data}) => {
            dispatch(Actions.setProject(data));
        });
    }
}

export default Actions;
