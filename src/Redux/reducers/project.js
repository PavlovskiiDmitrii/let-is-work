const initialState = []

export default (state = initialState, user) => {

    switch (user.type) {
        case 'PROJECT:SET':
            return {
                ...user.payload
            };
        default:
            return state
    }
}
