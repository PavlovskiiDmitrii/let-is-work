const initialState = localStorage.user ? JSON.parse(localStorage.getItem("user")) : { "auth": false }

export default (state = initialState, user) => {

    switch (user.type) {
        case 'USER:SET_ME':
            return {
                ...user.payload,
                auth: true
            };
        case 'USER:UNSET':
            return {
                ...user.payload,
                auth: false
            };
        default:
            return state
    }
}
